import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterElement.append(fighterImage, fighterInfo);
  }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });

  const elementsInfo = createElementsInfo(fighter);
  fighterInfo.append(...elementsInfo);

  return fighterInfo;
}

export function createElementsInfo(fighter) {
  return  Object.keys(fighter)
    .filter((element) => element !== '_id' && element !== 'source')
    .map((key) => {
      const element = createElement({
        tagName: 'div',
        className: `fighter-preview___${key}`,
        title: fighter[key]
      });
      element.innerHTML = `${key} - ${fighter[key]}`.toUpperCase();
      return element;
    });
}

