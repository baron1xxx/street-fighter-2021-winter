import {keyDownHandler, keyUpHandler} from '../../javascript/components/eventHandlers';
import {MAX_CRITICAL_HIT, MIN_CRITICAL_HIT} from '../../constants/hitPower';

export const gameState = {
    isPlayerOneBlockActive: false,
    isPlayerTwoBlockActive: false,
    firstFighterCurrentHealth: 0,
    secondFighterCurrentHealth: 0,
    playerOneCriticalHitCombinationArray: new Set(),
    playerTwoCriticalHitCombinationArray: new Set(),
    timePlayerOneCriticalHit: 0,
    timePlayerTwoCriticalHit: 0
};

export async function fight(firstFighter, secondFighter) {

    setInitialGameState(firstFighter, secondFighter);

    return new Promise((resolve) => {
        const keyUpListener = event => keyUpHandler(event);
        const keyDownListener = event => {
            const winner = keyDownHandler(event, firstFighter, secondFighter);
            if (winner) {
                document.removeEventListener('keydown', keyDownListener, false);
                document.removeEventListener('keyup', keyUpListener, false);
                resolve(winner)
            }
        };

        document.addEventListener('keydown', keyDownListener, false);
        document.addEventListener('keyup', keyUpListener, false);
    });
}

export function getDamage(attacker, defender) {
    // return damage
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
    // return hit power
    return fighter.attack * (Math.random() * (MAX_CRITICAL_HIT - MIN_CRITICAL_HIT) + MIN_CRITICAL_HIT);
}

export function getBlockPower(fighter) {
    // return block power
    return fighter.defense * (Math.random() * (MAX_CRITICAL_HIT - MIN_CRITICAL_HIT) + MIN_CRITICAL_HIT);
}

export function getCriticalHitPower(fighter) {
    return fighter.attack * 2;
}

export function updateHealthIndicator(position, fighter, fighterHealth) {
    const bar = document.getElementById(`${position}-fighter-indicator`);
    const healthFighter = (fighterHealth / fighter.health) * 100;
    bar.style.width = `${healthFighter}%`;
    if (healthFighter <= 20) {
        bar.style.backgroundColor = 'red';
    }
    if (healthFighter <= 0) {
        bar.style.width = '0';
    }
}

function setInitialGameState(firstFighter, secondFighter) {
    gameState.firstFighterCurrentHealth = firstFighter.health;
    gameState.secondFighterCurrentHealth = secondFighter.health;
    gameState.timePlayerOneCriticalHit = 0;
    gameState.timePlayerTwoCriticalHit = 0;
    gameState.isPlayerOneBlockActive = false;
    gameState.isPlayerTwoBlockActive = false;
    gameState.playerOneCriticalHitCombinationArray.clear();
    gameState.playerTwoCriticalHitCombinationArray.clear();
}
