import {controls} from '../../constants/controls';
import {LEFT_POSITION, RIGHT_POSITION} from '../../constants/fighterPosition';
import {getDamage, getCriticalHitPower, updateHealthIndicator, gameState} from './fight';

const {
    PlayerOneBlock,
    PlayerTwoBlock,
    PlayerOneAttack,
    PlayerTwoAttack,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination
} = controls;

export const keyDownHandler = (event, firstFighter, secondFighter) => {
    const {code} = event;
    switch (code) {
        case PlayerOneBlock:
            gameState.isPlayerOneBlockActive = true;
            break;
        case PlayerTwoBlock:
            gameState.isPlayerTwoBlockActive = true;
            break;
        case PlayerOneAttack:
            if (!gameState.isPlayerTwoBlockActive && !gameState.isPlayerOneBlockActive) {
                const damage = getDamage(firstFighter, secondFighter);
                // TODO setFirstFighterCurrentHealth() {return gameState.secondFighterCurrentHealth -= damage}  function
                gameState.secondFighterCurrentHealth -= damage;
                updateHealthIndicator(RIGHT_POSITION, secondFighter, gameState.secondFighterCurrentHealth);
            }
            break;
        case PlayerTwoAttack:
            if (!gameState.isPlayerTwoBlockActive && !gameState.isPlayerOneBlockActive) {
                const damage = getDamage(secondFighter, firstFighter);
                // TODO function setSecondFighterCurrentHealth() {return gameState.secondFighterCurrentHealth -= damage}
                gameState.firstFighterCurrentHealth -= damage;
                updateHealthIndicator(LEFT_POSITION, firstFighter, gameState.firstFighterCurrentHealth);
            }
            break;
    }
// PUSH CRITICAL HIT COMBINATION
    if (PlayerOneCriticalHitCombination.includes(code)) {
        gameState.playerOneCriticalHitCombinationArray.add(code);
    } else if (PlayerTwoCriticalHitCombination.includes(code)) {
        gameState.playerTwoCriticalHitCombinationArray.add(code);
    }

    if (gameState.playerOneCriticalHitCombinationArray.size === 3
        && gameState.timePlayerOneCriticalHit + 10000 < Date.now()) {
        gameState.timePlayerOneCriticalHit = Date.now();
        const damage = getCriticalHitPower(firstFighter);
        gameState.secondFighterCurrentHealth -= damage;
        updateHealthIndicator(RIGHT_POSITION, secondFighter, gameState.secondFighterCurrentHealth);
    } else if (gameState.playerTwoCriticalHitCombinationArray.size === 3
        && gameState.timePlayerTwoCriticalHit + 10000 < Date.now()) {
        gameState.timePlayerTwoCriticalHit = Date.now();
        const damage = getCriticalHitPower(secondFighter);
        gameState.firstFighterCurrentHealth -= damage;
        updateHealthIndicator(LEFT_POSITION, firstFighter, gameState.firstFighterCurrentHealth);
    }

    // return winner!!!
    if (gameState.firstFighterCurrentHealth <= 0) {
        return secondFighter;
    } else if (gameState.secondFighterCurrentHealth <= 0) {
        return firstFighter
    }

};

export const keyUpHandler = event => {
    const {code} = event;

    switch (code) {
        case PlayerOneBlock:
            gameState.isPlayerOneBlockActive = false;
            break;
        case PlayerTwoBlock:
            gameState.isPlayerTwoBlockActive = false;
            break;

    }

    // REMOVE CRITICAL HIT COMBINATION
    if (gameState.playerOneCriticalHitCombinationArray.has(code)) {
        gameState.playerOneCriticalHitCombinationArray.delete(code);
    }
    if (gameState.playerTwoCriticalHitCombinationArray.has(code)) {
        gameState.playerTwoCriticalHitCombinationArray.delete(code);
    }
};
